var d = new Date();
var minutes = d.getMinutes() + "";
var seconds = d.getSeconds() + "";
var str = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate() + " " + d.getHours() + ":" + minutes.padStart(2, '0') + ":" + seconds.padStart(2, '0');
document.getElementById("date").textContent = str;